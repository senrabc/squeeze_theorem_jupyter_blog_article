NOTES.txt
--------------------------------------------------------------------------------
So I had an idea to turn this idea into a paper so I could practice using TEX
and writing and formatting math and physics in this format, which is rapidly
approaching me as a required skill for upper level math and physics. So, this
morning while listening to an audiobook on Oppenheimer I thought to write
"A brief history of the squeeze theorem" since I wasn't able to find much about
the theorems origins until I looked in the Art of Computer Programming by
Donald Knuth. 
