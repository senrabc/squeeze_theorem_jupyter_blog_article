
\title{Writing Scientific Reports Using \LaTeX}
\author         {Fiz A. Cist}
\email          {einstein@mit.edu}
\homepage{http://web.mit.edu/8.13/}
\date{\today}
\affiliation{MIT Department of Physics}


\begin{abstract}
We present a written summary template for use by MIT Junior Lab
students, using \LaTeX and the {\bf RevTeX-4} macro package from the
American Physical Society.  This is the standard package used in
preparing most Physical Review papers, and is used in many other
journals as well.  The individual summary you hand in should show
evidence of your own mastery of the entire experiment, and possess a
neat appearance with concise and correct English.  The abstract is
essential.  It should briefly mention the motivation, the method and
most important, the quantitative result with errors.  Based on
those, a conclusion may be drawn.  The length of the paper should be
no more than 2 double-sided pages including all figures.  Appendices
can be use for plots of raw data but should not be used to simply
extend turgid prose!
\end{abstract}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Writing Scientific Papers}

One of the most important resources for developing into a strong
technical writer is the MIT Online Writing Center\cite{owc}.
Students should thoroughly investigate the resources on this site in
the first weeks of 8.13.  Note that students can receive free
`in-person' and `on-line' consultation on their written reports
through this office! 

An important part of your education as a physicist is learning to
use standard tools which enable you to share your work with others.
In Junior Lab, we will instruct you in the use of \LaTeX on either
MIT's Athena environment or your own personal Windows machine to
write scientific papers in a widely accepted professional style. The
source file
\footnote{\url{web.mit.edu/8.13/www/Samplepaper/sample-paper.tex}}
for this document may be used as a template for your Junior Lab
papers. Spending a few hours studying and altering this document
will allow you to develop sufficient mastery of \LaTeX to easily
generate all manner of technical documents.  Specific instructions
for compiling \LaTeX documents on Windows and Athena systems are
contained in the Appendices.  The Writing
Process\footnote{\url{web.mit.edu/writing/Resources/Writers/process.html}}
involves at least four distinct steps: prewriting, drafting,
revising and editing.  Given the tight time constraints in Junior
Lab, students are advised to begin the drafting process {\bf before}
finishing their lab sessions.  While final results and analysis are
not possible, much of the draft can be accomplished during the
latter sessions of an experiment.

The written report introduction should succinctly report the
motivation, purpose and relevant background to the experiment.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\subsection{Expository Writing}
 The essence of expository writing is the communication of understanding
through a clear and concise presentation of predominately factual
material.\cite{mayfield1998,pritchard1990} Most people cannot
compose successful expository prose unless they put the need to
communicate foremost among their priorities. Two things predominate
in generating understanding in the reader:
\begin{enumerate}
\item ORGANIZATION: The reader must be provided with an overview or
outline, know how each fact that he reads fits into that overall
picture, and he must be alerted if it is an especially important
fact. Furthermore, the facts must be presented in a logical order
(so that fact 17 is not important for understanding fact 12).

\item UNIFORM DEPTH of PRESENTATION: Bearing in mind the preexisting
knowledge of the reader, the writer must budget the length of
discussion allotted to each topic in proportion to its importance.

\end{enumerate}

Of course clarity of presentation and elegance of explanation will
greatly enhance the ease and pleasure of understanding; still, a
murky explanation can be fairly useful if the reader has been told
what he is reading about and where it fits into the overall scheme
of things - especially if the reader is familiar with the general
subject matter under discussion.

The Junior lab writeup is one of the few opportunities
undergraduates are given to practice technical writing. Thus we urge
you to concentrate on your overall presentation, not only on the
facts themselves. We strongly recommend that you:
\begin{enumerate}
\item Base your report on an outline.
\item Begin each paragraph with a topic sentence which expresses the
main area of concern and the main conclusion of the paragraph. Put
less important material later in the paragraph.
\end{enumerate}

Point 2 is frequently absent in 8.13 reports; they are your
mechanism for telling the reader what the topic under discussion is
and where it fits into the overall picture.

You can check your topic sentences by reading them in order (i.e.
omit all the following sentences in each paragraph) - this should
give a fair synopsis of your paper.

If you are individually writing up results you obtained with a
partner, use we and I appropriately.

Use the past tense for your procedure and analysis, the past perfect
for preparation and the present for emphasis or conclusions, e.g.
``Since we had previously measured constructive and destructive
interference, we concluded that electrons are waves.''

\begin{enumerate}
\item Be sure your Figures have comprehensible captions.

\item Make a complete estimate of your errors (not just statistical) - even
if it's crude.

\item Trace origin of formulae you use (e.g. Moseley's Law) to well known
physics (in this case to the Bohr atom) - don't derive, just
indicate what new assumptions are needed.
\end{enumerate}

Please consult the MIT's Online Writing and Communications Center's
web page\footnote{MIT Online Writing and Communication Office:
\url{web.mit.edu/writing/}} for further guidance in all aspects of
writing, style and to make appointments with consultants for free
advice.  They even have an on-line tutor to which you can submit
sections of your paper for critique at any stage of the writing
process!!!

{\bf Lastly: Remember to proofread your paper for spelling and grammar
 mistakes.  Few things are as offensive to a reviewer as careless
 writing and such mistakes will count against you!}

\section{Problem and Relevant Theory}

The report should be type-written in a form that would be suitable
for submission as a manuscript for publication in a professional
journal such as Physical Review Letters\footnote{Physical Review
Letters: \url{prl.aps.org/}}. One helpful resource is the APS
Physics Review Style and Notation Guide\footnote{ APS Physics Style
and Notation Guide: \url{publish.aps.org/STYLE/}}. Figures (created
as PDF files) should be inserted into the text in their natural
positions. The body of the summary should include a discussion of
the theoretical issues addressed by the experiment. This should be
done at a level, so that another 8.13 student could follow your
development.

\subsection{Typesetting Mathematics}

One of the great powers of \LaTeX is it's ability to typeset all
manner of mathematical expressions.  While it does take a short
while to get used to the syntax, it will soon become second nature.
Numbered, single-line equations are the most common type of equation
in \textit{Junior Lab papers} and are usually referenced in the
text; e.g. see Equation~(\ref{eq:first-equation}).
%
\begin{equation}
   \chi_+(p)\alt{\bf [}2|{\bf p}|(|{\bf p}|+p_z){\bf ]}^{-1/2}
   \left(
   \begin{array}{c}
      |{\bf p}|+p_z\\
      px+ip_y
   \end{array}\right)
\,. \label{eq:first-equation}
\end{equation}
%
% Be sure there is NO EMPTY LINE after \end{quation} and before the
% following lines, if you do not want a new paragraph to start there
% (and be indented).
%
Mathematics can also be placed directly in the text using
delimeters: $\vec{\psi_1} = |\psi_1\rangle \equiv c_0|0\rangle +
c_1|1\rangle \chi^2 \approx
\prod\sum\left[\frac{y_i-f(x_i)}{\sigma_i}\right]^2 |\psi_1\rangle
\sim \lim_{\mu \rightarrow \infty}p(x;\mu) \geq \frac{1}{\sqrt{2 \pi
\mu}} e^{-(x-\mu)^2 / 2\mu}P(x) \ll \int_{-\infty}^x p(x')dx'a
\times b \pm c \Rightarrow \nabla \hbar$.

Infrequently, you may wish to typeset long equations which span more
than one line of a two-column page.  A good solution is to split-up
the equation into multiple lines and label all with a single
equation number, like in Equation~\ref{eq:multilineeq}.  See the
\LaTeX file to see how this is done.
%
\begin{eqnarray}
  \sum \vert M^{\text{viol}}_g \vert ^2
   &=&  g^{2n-4}_S(Q^2)~N^{n-2} (N^2-1)
\nonumber
\\
   &&   \times \left( \sum_{i<j}\right) \sum_{\text{perm}}
            \frac{1}{S_{12}}  \frac{1}{S_{12}} \sum_\tau c^f_\tau
\,.
\label{eq:multilineeq}
\end{eqnarray}

Finally, it is often useful to group related equations to denote their
relationship, e.g. in a derivation.  Enclosing single-line and
multiline equations in \verb+\begin{subequations}+ and
\verb+\end{subequations}+ will produce a set of equations that are
``numbered'' with letters, as shown in Equations.~(\ref{subeq:1}) and
(\ref{subeq:2}) below:
\begin{subequations}
\label{eq:whole}
\begin{equation}
  \left\{
      abc123456abcdef\alpha\beta\gamma\delta1234556\alpha\beta
       \frac{1\sum^{a}_{b}}{A^2}
  \right\}
%
\,\label{subeq:1}
\end{equation}
\begin{eqnarray}
  {\cal M} &=& ig_Z^2(4E_1E_2)^{1/2}(l_i^2)^{-1}
                (g_{\sigma_2}^e)^2\chi_{-\sigma_2}(p_2)
\nonumber\\
  &&\times [\epsilon_i]_{\sigma_1}\chi_{\sigma_1}(p_1).\label{subeq:2}
\end{eqnarray}
\end{subequations}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Experimental Sketch and Details}

This section describes the main components of the apparatus,
procedures used and always makes reference to a figure(s) which
contains a block diagram or schematic of the apparatus and perhaps
includes the most important signal processing steps. {\bf The figure
should be referenced as early as possible in this section with the
placement of the figure as close to the descriptive text as is
possible.}  It is usually necessary to place additional information
within the figures themselves or in their captions for which there is
no room in the main body of text.  This will help you stay within the
two page limit.

{\bf Example first sentence of an experimental section}
The experimental apparatus consists of a specially prepared chemical
sample containing $^{13}$CHCl$_3$, a NMR spectrometer, and a control
computer, as shown in Figure~\ref{fig:samplefig}.

%
% Note, when including figures in a TeX document,
% we suggest you first create the figures as
% PDF files and then include them in the following way -
% without the .pdf suffix)
%

\begin{figure}[htb]
\includegraphics[width=8cm]{sample-fig1}
\caption{This is a schematic of the main apparatus.  Use the caption
space to elaborate on specific issues or complication, or operating
procedures.  Especially valuable given the limited about of space in
the main body of text.  The size of this graphic was set by the width
command, the aspect ratio defaults to 1.0 if the height is not also
set. Adapted from \cite{melissinos1966,melissinos2003}.\label{fig:samplefig}}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Data Presentation and Error Analysis}

All papers should have at least one graphic showing some assemblage
of raw data often times placed as an appendix, see for example
Fig.~\ref{fig:landscapegraphic}. Often these primary data are
analyzed in a specific way that needs to be clearly communicated to
the reader.  In many physics experiments, the peak positions in a
energy spectrum may be required. A graphic demonstrating a typical
fit result, functional model, reduced $\chi^2$ is shown in
Fig.~\ref{fig:calibration}. Finally, there should be one graphic
which summarizes the experimental data, and which conveys primary
finding(s) of the laboratory exercise (e.g. the Geiger-Nuttall
relationship in Fig~\ref{fig:frenchtaylor}, Moseley's Law, the
Rotation curve of the Milky Way, the Compton Scattering Energies vs.
Angle, etc. You may find that you need more but these three should
be a minimum. Finally, it can be useful in some circumstances to
have a table of results, see Table~\ref{tab:table1}

Graphics, such as Figure~\ref{fig:calibration} should be well
thought out and crafted to maximize their information content while
retaining clarity of expression!  If you `reuse' graphics from your
paper in oral presentation slides, make sure to increase the size of
all the fonts so that they remain legible from 20 feet away!

\begin{figure}[htb]
\includegraphics[width=9cm]{sample-fig2.pdf}
\caption{Sample figure describing a set of data, fit procedures and
results. Use the caption space to provide more details about the
fitting procedure, results or implications if you do not have
sufficient room in the main body of text. This figure was created
using the Matlab script at
\url{web.mit.edu/8.13/matlab/fittemplate07.m}}
\label{fig:calibration}
\end{figure}


%\begin{figure*}[htb]
%\includegraphics[angle=0,width=10cm]{sample-fig3}
%\caption{Sample paneled figure created in Matlab using the
%subplot(2,2,x) command where x is the element of the plot array into
%which all subsequent commands such as plot(x,y) and xlabel('Volts'),
%etc. get processed.  Use the caption space to provide more details
%about the data, their acquisition or how they were processed if you do
%not have sufficient room in the main body of text.  Figures can be
%rotated using the angle command, see the TeX file for details.  If a
%figure is to be placed after the main text use the ``figure*'' option
%to make it extend over two columns, see the \LaTeX file for how this
%was done.}
%\label{fig:panel2x2}
%end{figure*}

\begin{figure}[htb]
\includegraphics[width=9cm]{frenchtaylor.pdf}
\caption{Sample figure showing overall physical relationship you set
out to test.  This figure was created using the Matlab script at
\url{web.mit.edu/8.13/matlab/fittemplate07.m}}
\label{fig:frenchtaylor}
\end{figure}

Try to avoid the temptation to inundate the reader with too many
graphics.  It is worth spending some time thinking of how best to
present information rather than just creating graph after graph of
uninformative data.  All figures and tables must be properly
captioned.  Material and ideas drawn from the work of others must be
properly cited, and a list of references should be included at the end
of the text but before the graphics.

\begin{table}[h]
\caption{\label{tab:table1}A example table with footnotes.  Note
that several entries share the same footnote. Always use a preceding
zero in the data you record in tables.  Always display UNITS.
 Inspect the \LaTeX\ input for this table to see exactly how it is
done.}
\begin{ruledtabular}
\begin{tabular}{cccccccc}
 &$r_c$ (\AA)&$r_0$ (\AA)&$\kappa r_0$&
 &$r_c$ (\AA) &$r_0$ (\AA)&$\kappa r_0$\\
\hline
Cu& 0.800 & 14.10 & 2.550 &Sn\footnotemark[1] & 0.680 & 1.870 & 3.700 \\
Ag& 0.990 & 15.90 & 2.710 &Pb\footnotemark[1] & 0.450 & 1.930 & 3.760 \\
Tl& 0.480 & 18.90 & 3.550 & & & & \\
\end{tabular}
\end{ruledtabular}
\footnotetext[1]{Here's the first, from Ref.~\cite{bevington2003}.}
\end{table}

If circumstances in an experiment are such that you cannot get your
own data (e.g. broken equipment, bad weather), {\bf you may use
somebody else's data provided you acknowledge it}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions}

And finally, conclusions.  Remember to report all your results with
appropriate significant digits, units, and uncertainties, e.g. Q =
(2.12 $\pm$ 0.06) disintegrations s$^{-1}$.  It is often very useful
to express the quality of your result by measuring how many standard
deviations it lies from other published values.

It is worth mentioning here some thoughts on {\bf ethics and writing
in Science}.

When you read the report of a physics experiment in a reputable
journal (e.g. Physical Review Letters) you can generally assume it
represents an honest effort by the authors to describe exactly what
they observed. You may doubt the interpretation or the theory they
create to explain the results. But at least you trust that if you
repeat the manipulations as described, you will get essentially the
same experimental results.

Nature is the ultimate enforcer of truth in science. If subsequent
work proves a published measurement is wrong by substantially more
than the estimated error limits, a reputation shrinks. If fraud is
discovered, a career may be ruined. So most professional scientists
are very careful about the records they maintain and the results and
errors they publish.

In keeping with the spirit of trust in science, Junior Lab instructors
will assume that what you record in your lab book and report in your
written and oral presentations is exactly what you have observed.

{\bf Fabrication or falsification of data, using the results of
another person's work without acknowledgement, or copying from {\em
``living group files''} are intellectual crimes as serious as
plagiarism, and possible causes for dismissal from the Institute.}

{\bf The acknowledgement of other people's data also applies to the use
of other people's rhetoric.} The appropriate way to incorporate an
idea which you have learned from a textbook or other reference is to
study the point until you understand it and then put the text aside
and state the idea in your own words.

One often sees, in a scientific journal, phrases such as ``Following
Bevington and Melissinos \cite{bevington2003, melissinos1966} ...''
This means that the author is following the ideas or logic of these
authors and not their exact words.

If you do choose to quote material, it is not sufficient just to
include the original source among the list of references at the end of
your paper. If a few sentences or more are imported from another
source, that section should be

\begin{quote}indented on both sides or enclosed in
quotes, and attribution must be given immediately in the form of a
reference note.\cite{melissinos1966}
\end{quote}

If you have any question at all about attribution of sources, please
see you section instructor.

Further information about how to avoid plagiarism is available
online at \url{web.mit.edu/writing/Citation/plagiarism.html}.
\section{Bibliography Remarks}
Bibliographies are very important in Junior Lab papers.  Beyond the
requisite citation of source material, they provide evidence of your
investigations beyond the narrow scope of the labguide, something
explicitly required of all Junior Lab students!  Good bibliographies
are doubly important in the real world where they are very (often
the most) important sources of information for researchers entering
the field.  Bibliographic entries are made within a separate `.bib'
file which gets attached during process of building a final PDF
document.  See the file
\url{web.mit.edu/8.13/www/Samplepaper/sample-paper.bib} for details
on several types of bibliographic entries and their required and
optional fields.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Place all of the references you used to write this paper in a file
% with the same name as following the \bibliography command
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\bibliography{sample-paper}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{acknowledgments} FAC gratefully acknowledges Dr. Francine Brown for
her early reviews of this manuscript.
\end{acknowledgments}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage
\appendix
\section{\LaTeX~ Under Windows}
For those students who would like to use a Windows platform, MiKTeX
(pronounced \emph{mik-tech} is a freely available, implementation of
TeX and related programs available from \url{www.miktex.org}. Note
that MiKTeX itself runs from a command line prompt and is not terribly
convenient.  We strongly recommend you simultaneously purchase and
install a very nice TeX editor/shell called WinEdt, available from
\url{www.winedt.com} for only \$30 for students. This interface is
substantially easier than using `emacs' on Athena for writing and
typesetting scientific papers and we encourage you to check it out.

Once you've installed the above software, you will need to obtain
the group of files listed in the next section and put them on your
Windows machine in order to `rebuild' this document from scratch.


If you wish to view postscript files under Windows, we
suggest downloading and installing Ghostscript available from
\url{www.cs.wisc.edu/~ghost}.

\section{\LaTeX~ on Athena}
For students wishing to utilize MIT's Athena environment, it is also
a simple process to create your documents.  You can use the following
commands verbatim or tweak them to suit your own organizational system.

In your home directory on Athena, create a convenient directory structure for all of your Junior Lab
work. Type:
\begin{verbatim}
> mkdir ~/8.13
> mkdir ~/8.13/papers
> mkdir ~/8.13/papers/template
> cd ~/8.13/papers/template
\end{verbatim}
Once this (or similiar) directory structure has been created, copy all
of the files needed to compile the template from the Junior Lab locker
into your own Athena account: Type:
\begin{verbatim}
> setup 8.13
> cp /mit/8.13/www/Samplepaper/* .
\end{verbatim}
The final period above places the
copied files into the current directory so make sure you're in the
correct directory!  You can see where you are by typing:
\begin{verbatim}
> pwd
\end{verbatim}
The following files should now be in
your current directory:
\begin{verbatim}
sample-paper.tex
sample-paper.bib
sample-fig1.pdf
sample-fig2.pdf
sample-fig3.pdf
typical-fit-plot.pdf
\end{verbatim}
Additional files may also have been copied but don't worry, these get
regenerated when you build your PDF document.

The `setup' command automatically
appends to your path the location of the {\bf RevTeX-4} files.

Now let's build the file (omitting the `.tex' suffix in the following steps).

\begin{verbatim}
> pdflatex sample-paper
> bibtex sample-paper
> pdflatex sample-paper
> pdflatex sample-paper
\end{verbatim}


The repeated calls to `pdflatex' are necessary to resolve any nested
references in the final PDf file.  The `bibtex' call reads in the
bibliography file `sample-paper.bib' allowing citation references to
be resolved.

{\bf Remember to {\tt ispell -t filename.tex} to perform a \LaTeX
safe spell check before handing in your paper!}

\section{Useful Athena Utilities}
{\bf Drawing Programs}

Students should become proficient with a simple (vector based)
computer drawing program such as {\bf XFIG} or {\bf TGIF} on Athena.
Every written summary should include one or two simple schematics,
based on their initial hand sketches from their lab notebooks.

{\bf Image Conversion}

It is easy to  convert images from one format to another (e.g. a
scanned jpeg or bitmap image into an pdf file for inclusion into a
written summary).  A useful utility, available on the Sun's is
``imconvert''. Typing ``imconvert'' without any arguments will show
you the accepted file types.  For example, to convert a `jpg' image
to `pdf', one types: ``imconvert jpg:filename.jpg
pdf:filename.pdf''.  Other useful commands are `ps2pdf' and `eps2pdf'.

\section{Matlab and \LaTeX}
Matlab is perhaps the most common tool used by Junior Lab students
for data analysis and representation.  Matlab figures can
incorporate \LaTeX symbols in their titles, axes labels and text
labels.  Figures can be saved directly into a `PDF' format obviating
the need for any further format translation.


% Surround figure environment with turnpage environment for landscape presentation
\begin{turnpage}
\begin{figure*}[htb]
\includegraphics[width=20cm]{sample-fig3}
\caption{For very large plots where important detail might be lost
if too compressed, it can be convenient to use the `turnpage'
environment for displaying in landscape mode. e.g. any experiment
where a data set is acquired at several angular positions (21cm,
e/m, Rutherford) or is time varying (Physics of Alpha Decay and
Pulsed NMR.)  These full page graphics are usually best kept in
appendices so as not to impede the flow of the paper.  Note that
large tables can also be presented in this landscape environment if
desired \label{fig:landscapegraphic}}
\end{figure*}
\end{turnpage}


% To convert program (e.g. C++ Fortran, Matlab, LaTeX) listings to a
% form easily includable in a LaTeX document
%
% type lgrind -s to see options
% lgrind -llatex -i sample-paper.tex > sampleinputtex
% creates a file sampleinput.tex which can then be included into this
% document simply by uncommenting the next line
%\lgrindfile{testinput.tex}
